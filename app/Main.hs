{-# LANGUAGE RecordWildCards, OverloadedStrings #-}
import Text.Pandoc
import Text.Read
import Pipes.Core
import Text.Pandoc.Error
import Network.Discord
import System.Environment
import Control.Concurrent.STM
import System.IO.Unsafe
import Control.Monad (when)
import Data.Text as T hiding (drop, filter)
import Data.Time.Clock
import System.Log.Logger

readerOpts :: ReaderOptions
readerOpts = def { readerStandalone  = False
                 , readerApplyMacros = True
                 }

writerOpts :: WriterOptions
writerOpts = def { writerTeXLigatures = True
                 , writerListings     = True
                 }

owner :: TVar Snowflake
owner = unsafePerformIO $ newTVarIO 0

latexToMd :: String -> Either PandocError String
latexToMd input = readLaTeX readerOpts (filter (/= '\r') input)
  >>= (return . writeHtmlString writerOpts)
  >>= readHtml readerOpts
  >>= (return . writePlain writerOpts)

edit :: Message -> Text -> Maybe Embed -> Effect DiscordM ()
edit msg txt embed = fetch' $ EditMessage msg txt embed

data ParseEmbed = E Integer String String [SubEmbed] deriving (Show, Eq, Read)

baseEmbed :: Embed
baseEmbed = Embed
  { embedTitle  = ""
  , embedType   = ""
  , embedDesc   = ""
  , embedUrl    = ""
  , embedTime   = undefined
  , embedColor  = 0
  , embedFields = []
  }

makeEmbed :: ParseEmbed -> Either String Embed
makeEmbed (E color name desc subs)
  = Right baseEmbed
    { embedTitle  = name
    , embedDesc   = desc
    , embedColor  = color
    , embedFields = subs
    }

main :: IO ()
main = do
  updateGlobalLogger "Discord-hs" (setLevel INFO)
  updateGlobalLogger "Texbot" (setLevel INFO)
  token <- getEnv "DISCORD_TOKEN"
  runBot (Client token) $ do
    with ReadyEvent $ \(Init v (User {..}) _ _ _) -> do
      liftIO . infoM "Texbot" $ "Connected to gateway v" ++ show v
      liftIO . atomically $ writeTVar owner userId

    with MessageCreateEvent $ \msg@Message{..} -> do
      me <- liftIO $ readTVarIO owner
      when (userId messageAuthor == me) $ do
        liftIO $ infoM "Texbot" $ show msg
        when (".tex" `isPrefixOf` messageContent) $ do
          liftIO $ infoM "Texbot" "Parsing latex"
          case latexToMd . drop 4 $ unpack messageContent of
            Right res -> edit msg (pack res) Nothing
            Left  err -> liftIO $ print err
        when (".embed" `isPrefixOf` messageContent) $ do
          liftIO $ infoM "Texbot" "Parsing embed"
          now <- liftIO getCurrentTime
          case makeEmbed =<< (readEither . drop 6 $ unpack messageContent) of
            Left  err   -> edit msg (pack err) Nothing
            Right embed -> edit msg "" $ Just embed {embedTime = now}
